<?php

function FetchDataList($filters, $offset = 0, $limit = -1, $type="list")
{
	global $db;
	$params = array();
	$where = '';
	$limit  = ($limit == -1) ? PHP_INT_MAX : $limit; 
	if (!empty($filters) && $filters[2]) 
	{
		$where .= " and {$filters[0]}";
		$params[] = $filters[2];
		switch ($filters[1]) {
			case "eq":
				$where .= " = '%s'";
				break;
			case "contain":
				$where .= " like '%%%s%%'";
				break;
			case "more":
				$where .= " > '%s'";
				break;
			case "less":
				$where .= " < '%s'";
				break;
		}
	}
	if ($type == "list" && $result = mysqli_query($db, vsprintf("SELECT * FROM data WHERE 1=1 $where LIMIT $limit OFFSET $offset", $params))) 
		return (mysqli_fetch_all($result, MYSQLI_ASSOC));
	else if ($type == "count" && $result = mysqli_query($db, vsprintf("SELECT count(id) count FROM data WHERE 1=1 $where LIMIT $limit OFFSET $offset", $params))) 
		return (mysqli_fetch_array($result, MYSQLI_ASSOC)["count"]);
	else
		return false;

}