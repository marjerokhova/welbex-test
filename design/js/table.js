/* Переменные для отрисовки таблицы */
table =  $("#table");
table_body = $("#table tbody");
table_row = $("<tr></tr>");
table_col = $("<td></td>");
/* */
/* Переменные для пагинации */
paginate = $("#paginate");
paginate_btn = $(".paginate_btn");
paginate_first = $("#paginate a[sort='first']");
paginate_page1 = $("#paginate a[sort='page1']");
paginate_page2 = $("#paginate a[sort='page2']");
paginate_page3 = $("#paginate a[sort='page3']");
paginate_last = $("#paginate a[sort='last']");
paginate_count = $("#paginate_count");
data_count = $("#data_count");
paginate_limit = $("#paginate_limit");
limit_record = paginate_limit.val();
cur_page = 1;
count_page = 1;
/* */
/* Переменные для фильтрации */
filters = $(".filters");
filter_field = $("#select_field");
filter_fields = $("#select_field option");
filter_ifs = $("#select_if option");
filter_value = $("#select_value");
filter_value_error = $("#select_value_error");
filter_btn = $("#filter_btn");
filter_cancel_btn = $("#filter_cancel_btn");

/* */

function load_data(data){
	table_body.empty();
	var request = $.get("/actions/data.php", { type: "data", limit: limit_record, page: cur_page, filter: data })
		.done(function(data) {
			if (data !== false)
			{
				var data_arr = JSON.parse(data);
				data_arr.forEach(function(row, i, data_arr) {
					new_row = table_row.clone();
					for( var col in row ){
						new_col = table_col.clone();
						new_col.html(row[col]);
						new_col.appendTo(new_row);
					}
					new_row.appendTo(table_body);
				});
			}
		});
}

function load_paginate(){
	var request = $.get("/actions/data.php", { type: "count"})
		.done(function(count) {
			if (count > limit_record)
			{
				count_page = Math.ceil(count/limit_record);
				paginate.removeClass("hidden");
				if (count_page >= 3)
				{
					page1 = (cur_page != 1 && cur_page != count_page) 
							? cur_page-1
							: ((cur_page == 1) 
								? cur_page 
								: cur_page-2
							);
					paginate_page1.html(page1);
					paginate_page1.removeClass("hidden");
					page2 = (cur_page != 1 && cur_page != count_page) 
							? cur_page
							: ((cur_page == 1) 
								? cur_page+1 
								: cur_page-1
							);
					paginate_page2.html(page2);
					paginate_page2.removeClass("hidden");
					page3 = (cur_page != 1 && cur_page != count_page) 
							? cur_page+1
							: ((cur_page == 1) 
								? cur_page+2 
								: cur_page
							);
					paginate_page3.html(page3);
					paginate_page3.removeClass("hidden");
				}
				else
				{
					paginate_page1.html(1);
					paginate_page1.removeClass("hidden");
					if (count_page != 1)
					{
						paginate_page2.html(count_page);
						paginate_page2.removeClass("hidden");
					}
					else
						paginate_page2.addClass("hidden");
					paginate_page3.addClass("hidden");
				}
			}
			else
			{
				paginate.addClass("hidden");
			}
			paginate_count.html(count_page);
			data_count.html(count);
		});
}
load_data();
load_paginate();

paginate_btn.on("click", function(){
	prev_page = cur_page;
	switch ($(this).attr("sort")) {
		case "first":
			cur_page = 1;
			break; 
		case "last":
			cur_page = count_page;
			break;
		default:
			cur_page = $(this).html()*1;
			break;
	}
	if (prev_page != cur_page) load_data();
	load_paginate();
});

paginate_limit.on("keyup", function() {
	if (this.value.match(/[^0-9]/g)) 
		this.value = this.value.replace(/[^0-9]/g, '');
});

paginate_limit.on("change", function() {
	if (paginate_limit.val()) {
		limit_record = paginate_limit.val();
		cur_page = 1;
		load_paginate();
		load_data();	
	}
});

filter_field.on("change", function(){
	filter_value.val();
	filter_value_error.addClass("hidden");
	switch (filter_fields.filter(":selected").val()) {
		case "date":
			filter_value[0].type = "date";
			break; 
		default:
			filter_value[0].type = "text";
			break;
	}
});
filter_field.change();

filter_value.on("keyup", function() {
	filter_value_error.addClass("hidden");
	if (filter_fields.filter(":selected").val() == "count")
		if (this.value.match(/[^0-9]/g)) 
			this.value = this.value.replace(/[^0-9]/g, '');

	if (filter_fields.filter(":selected").val() == "dist")
		if (this.value.match(/[^0-9\.]/g)){
			this.value = this.value.replace(/,/g, '.');
			this.value = this.value.replace(/[^0-9\.]/g, '');
		}
});

filter_btn.on("click", function(){
	if (filter_value.val())
	{
		filter_arr=[filter_fields.filter(":selected").val(), filter_ifs.filter(":selected").val(), filter_value.val() ];
		cur_page = 1;
		load_data(filter_arr);
	}
	else
	{
		filter_value_error.removeClass("hidden");
		$('html, body').animate({ scrollTop: filter_value_error.offset().top }, 'slow');
	}
});

filter_cancel_btn.on("click", function(){
	filter_value_error.addClass("hidden");	
	load_data();
});
