<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кисина.Тест</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body class="bg-light">
	<div class="container">		
		<h2 class="py-5 text-center">Кисина М.А. Тестовое</h2>
		<fieldset>
			<legend>Фильтры</legend>
			<div class="form-group">
				<label for="select_field">Выберите колонку для фильтрации</label>
				<select id="select_field" class="form-control filters">
					<option value="date">Дата</option>
					<option value="name">Название</option>
					<option value="count">Количество</option>
					<option value="dist">Расстояние</option>
				</select>
			</div>
			<div class="form-group">
				<label for="select_if">Выберите условие для фильтрации</label>
				<select id="select_if" class="form-control filters">
					<option value="eq">Равно</option>
					<option value="contain">Содержит</option>
					<option value="more">Больше</option>
					<option value="less">Меньше</option>
				</select>
			</div>
			<div class="form-group">
				<label for="select_value">Введите значение для фильтрации</label>
				<input type="date" id="select_value" class="form-control filters" placeholder="Значение для фильтрации">
			</div>		
			<div class="alert alert-warning hidden" role="alert" id="select_value_error">
				Введите значение для фильтрации.
			</div>
			<button class="btn btn-primary" type="button" id="filter_btn">Фильтровать</button>
			<button class="btn btn-primary" type="button" id="filter_cancel_btn">Показать все</button>
		</fieldset>
		<hr>
		<table class="table" id="table"><caption>Данные:</caption>
			<thead>
				<tr>			
					<th>#</th>
					<th>Дата</th>
					<th>Название</th>
					<th>Количество</th>
					<th>Расстояние</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<ul id="paginate" class="pagination hidden">	
			<li class="page-item"><a class="page-link paginate_btn" sort="first" href="#">Первая</a></li>
			<li class="page-item"><a class="page-link paginate_btn hidden" sort="page1" href="#"></a></li>
			<li class="page-item"><a class="page-link paginate_btn" sort="page2"href="#">1</a></li>
			<li class="page-item"><a class="page-link paginate_btn hidden" sort="page3" href="#"></a></li>
			<li class="page-item"><a class="page-link paginate_btn" sort="last" href="#">Последняя</a></li>
		</ul>
		<fieldset class="form-inline">
			<div class="form-group">
				<label for="paginate_count">Всего страниц:</label>
				<u id="paginate_count"></u>
			</div>
			<div class="form-group">
				<label for="data_count">Всего записей:</label>
				<u id="data_count"></u>
			</div>
			<div class="form-group">
				<label for="paginate_limit">Лимит на странице:</label>
				<input type="text" id="paginate_limit" class="form-control" placeholder="" value="2">
			</div>	
		</fieldset>		
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="/design/js/table.js"></script>
  </body>
</html>
<?php
	
?>