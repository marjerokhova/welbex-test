<?php
require_once('../config/db_connect.php');
require_once('../classes/data.php');

if (isset($_GET['type']) && $_GET['type'] == 'data')
{
	$limit = (isset($_GET['limit'])) ? $_GET['limit'] : -1;
	$offset = (isset($_GET['page']) && isset($_GET['limit'])) ? ($_GET['page']-1)*$limit : 0;
	echo json_encode(FetchDataList($_GET['filter'], $offset, $limit));
}	

if (isset($_GET['type'])&& $_GET['type'] == 'count')
	echo FetchDataList(array(), 0, -1,"count");
